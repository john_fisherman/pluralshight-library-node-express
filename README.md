# Library App built with node.js and Express
_Following the Pluralsight course administered by Jonathan Mills._

This features:

* a modern JS stack using multiple plumbing techniques
* a templating system (different ones are discussed, the chosen one becomes EJS)
* MongoDB
* Routing

I coded this myself, feel free to reuse any parts. 

Fred Rocha, john.fisherman@gmail.com or [@john_fisherman](http://twitter.com/john_fisherman).